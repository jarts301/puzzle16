
/*
 * 
 * Autor Julio Cesar Sanchez Torres 2879351
 * 
 */

package sanchez.julio.puzzle;

import java.util.Random;

public class Puzzle {

	public static void main(String[] args) {

//		for (int i = 0; i < 10; i++) {
			String ti ="14,8,0,5,3,6,15,10,11,2,1,4,12,13,7,9";
			System.out.println("\n*******************************************************");
			System.out.println("\nTablero inicial: " + ti);
			Tablero tableroInicial = new Tablero(ti, 0.0, 0.0);
			Tablero tableroObjetivo = new Tablero("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0", 0.0, 0.0);

			System.out.println("\nA*(manhattan)");
			Heuristica heuristica = new Heuristica();
			Astar astar = new Astar(heuristica, "manhattan");
			tableroInicial.setCostoAproximado(heuristica.manhattan(tableroInicial.getContenido().split(","),
					tableroObjetivo.getContenido().split(",")));

			long time_start = System.currentTimeMillis();
			astar.buscarObjetivo(tableroInicial, tableroObjetivo);
			long time_fin = System.currentTimeMillis();
			System.out.println("Tiempo: " + (time_fin - time_start) + " milisegundos");

			System.out.println("\nA*(rabon)");
			astar = new Astar(heuristica, "rabon");
			tableroInicial.setCostoAproximado(heuristica.rabon(tableroInicial.getContenido().split(","),
					tableroObjetivo.getContenido().split(",")));

			time_start = System.currentTimeMillis();
			astar.buscarObjetivo(tableroInicial, tableroObjetivo);
			time_fin = System.currentTimeMillis();
			System.out.println("Tiempo: " + (time_fin - time_start) + " milisegundos");
			
//		}

		// IDS ids = new IDS();
		// ids.buscarObjetivo(tableroInicial, tableroObjetivo);

	}

	public static String tableroAleatorio() {

		int n = 16; // numeros aleatorios
		int k = n; // auxiliar;
		int[] numeros = new int[n];
		String resultado = "";
		Random rnd = new Random();
		int res;

		for (int i = 0; i < n; i++) {
			numeros[i] = i;
		}

		for (int i = 0; i < n; i++) {
			res = rnd.nextInt(k);
			resultado = resultado + numeros[res] + ",";
			numeros[res] = numeros[k - 1];
			k--;

		}
		return resultado.substring(0, resultado.length() - 1);
	}

}
