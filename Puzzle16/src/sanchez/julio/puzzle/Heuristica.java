/*
 * 
 * Autor Julio Cesar Sanchez Torres 2879351
 * 
 */

package sanchez.julio.puzzle;

import java.util.ArrayList;

public class Heuristica {
	
	public static ArrayList<String> coordenadas;
	
	public Heuristica(){
		generarCoordenadas();
	}
	
	public Double manhattan(String[] tablero, String[] objetivo) {
		Double resultado = 0.0;

		Integer indiceOrigen;
		Integer indiceDestino;

		String[] puntoUno;
		String[] puntoDos;

		Double calculo = 0.0;

		for (int i = 0; i < objetivo.length; i++) {
			if (!objetivo[i].equals(tablero[i])) {
				indiceOrigen = encontrarIndice(tablero, objetivo[i]);
				indiceDestino = i;
				puntoUno = coordenadas.get(indiceOrigen).split(",");
				puntoDos = coordenadas.get(indiceDestino).split(",");

//				calculo = Math.sqrt(Math.pow(Double.parseDouble(puntoUno[0]) - Double.parseDouble(puntoDos[0]), 2)
//						+ Math.pow(Double.parseDouble(puntoUno[1]) - Double.parseDouble(puntoDos[1]), 2));
				
				calculo = Math.abs(Double.parseDouble(puntoUno[0]) - Double.parseDouble(puntoDos[0])) +
						  Math.abs(Double.parseDouble(puntoUno[1]) - Double.parseDouble(puntoDos[1]));

				resultado = resultado + calculo;
			}
		}

		return resultado;
	}
	
	public Double rabon(String[] tablero, String[] objetivo){
		Double resultado = 0.0;
		
		for (int i = 0; i < objetivo.length; i++) {
			if(!objetivo[i].equals(tablero[i])){
				resultado++;
			}
		}
		
		return resultado;
	}
	
	public Double ninguna(){
		return 0.0;
	}
	
	public static Integer encontrarIndice(String[] cadenaTablero, String dato) {
		Integer resultado = 0;

		for (Integer j = 0; j < cadenaTablero.length; j++) {
			if (cadenaTablero[j].equals(dato)) {
				resultado = j;
				break;
			}
		}

		return resultado;
	}
	
	public static void generarCoordenadas() {
		// 16 cuadrados representados como coordenadas
		coordenadas = new ArrayList<String>();
		coordenadas.add("1,1");
		coordenadas.add("1,2");
		coordenadas.add("1,3");
		coordenadas.add("1,4");
		coordenadas.add("2,1");
		coordenadas.add("2,2");
		coordenadas.add("2,3");
		coordenadas.add("2,4");
		coordenadas.add("3,1");
		coordenadas.add("3,2");
		coordenadas.add("3,3");
		coordenadas.add("3,4");
		coordenadas.add("4,1");
		coordenadas.add("4,2");
		coordenadas.add("4,3");
		coordenadas.add("4,4");
	}

}
