/*
 * 
 * Autor Julio Cesar Sanchez Torres 2879351
 * 
 */

package sanchez.julio.puzzle;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

public class Astar {

	public Heuristica heuristica;
	public String[] objetivo;
	public PriorityQueue<Tablero> tablerosPosibles;
	public ArrayList<Tablero> tablerosVisitados;
	public Integer contador =0;
	public String nombreHeuristica="";

	public Astar(Heuristica heuristica, String nombreHeuristica) {
		this.heuristica = heuristica;
		this.nombreHeuristica=nombreHeuristica;
		tablerosPosibles = new PriorityQueue<Tablero>(11, new Comparator<Tablero>() {
			public int compare(Tablero t1, Tablero t2) {
				if (t1.getCostoTotal() < t2.getCostoTotal())
					return -1;
				if (t1.getCostoTotal() > t2.getCostoTotal())
					return 1;
				return 0;
			}
		});
		tablerosVisitados = new ArrayList<Tablero>();
	}

	public void buscarObjetivo(Tablero inicial, Tablero objetivo) {
		this.objetivo = objetivo.getContenido().split(",");
		Tablero tabActual = inicial;
		tablerosPosibles.add(tabActual);
		while (true) {
			contador++;
			tabActual = tablerosPosibles.poll();
//			System.out.println(tabActual.getContenido() + "--" + tabActual.getCostoTotal());
			if (esObjetivo(tabActual, objetivo)) {
//				System.out.println(contarPadres(tabActual));
				break;
			}
			calcularPosibilidades(tabActual);
			if(!tableroVisitado(tabActual)){
				tablerosVisitados.add(tabActual);
			}
		}
		System.out.println("Tableros revisados:"+contador);

	}

	public void calcularPosibilidades(Tablero tablero) {

		String[] cadenaTablero = tablero.getContenido().split(",");
		Integer indiceEspacio = encontrarEspacio(cadenaTablero);
		String nuevoTablero = "";
		Double costoReal = 0.0;
		Double costoEstimado = 0.0;
		Tablero tableroCalculado=null;
		// Arriba
		if (indiceEspacio - 4 >= 0) {
			nuevoTablero = generarTablero(cadenaTablero, indiceEspacio, indiceEspacio - 4);
			costoReal = tablero.getCostoReal() + costoReal(cadenaTablero, nuevoTablero.split(","));
//			costoReal = tablero.getCostoReal() + costoReal(cadenaTablero, nuevoTablero.split(","));
			if(nombreHeuristica.equals("manhattan")){
				costoEstimado = heuristica.manhattan(nuevoTablero.split(","), objetivo);
			}else
			if(nombreHeuristica.equals("rabon")){
				costoEstimado = heuristica.rabon(nuevoTablero.split(","), objetivo);
			}else
			if(nombreHeuristica.equals("ninguna")){
				costoEstimado = heuristica.ninguna();
			}
			tableroCalculado = new Tablero(nuevoTablero, costoEstimado, costoReal);
			tableroCalculado.setPadre(tablero);
			if (!tableroVisitado(tableroCalculado)) {
				tablerosPosibles.add(tableroCalculado);
			}
//			System.out.println(nuevoTablero);
		}
		// Abajo
		if (indiceEspacio + 4 <= 15) {
			nuevoTablero = generarTablero(cadenaTablero, indiceEspacio, indiceEspacio + 4);
//			costoReal = tablero.getCostoReal() + costoReal(nuevoTablero.split(","), objetivo);
			costoReal = tablero.getCostoReal() + costoReal(cadenaTablero, nuevoTablero.split(","));
			if(nombreHeuristica.equals("manhattan")){
				costoEstimado = heuristica.manhattan(nuevoTablero.split(","), objetivo);
			}
			if(nombreHeuristica.equals("rabon")){
				costoEstimado = heuristica.rabon(nuevoTablero.split(","), objetivo);
			}
			if(nombreHeuristica.equals("ninguna")){
				costoEstimado = heuristica.ninguna();
			}
			tableroCalculado = new Tablero(nuevoTablero, costoEstimado, costoReal);
			tableroCalculado = new Tablero(nuevoTablero, costoEstimado);
			tableroCalculado.setPadre(tablero);
			if (!tableroVisitado(tableroCalculado)) {
				tablerosPosibles.add(tableroCalculado);
			}
//			System.out.println(nuevoTablero);
		}
		// Izquierda
		if (indiceEspacio - 1 >= 0) {
			if (indiceEspacio != 0 && indiceEspacio != 4 && indiceEspacio != 8 && indiceEspacio != 12) {
				nuevoTablero = generarTablero(cadenaTablero, indiceEspacio, indiceEspacio - 1);
				costoReal = tablero.getCostoReal() + costoReal(cadenaTablero, nuevoTablero.split(","));
				if(nombreHeuristica.equals("manhattan")){
					costoEstimado = heuristica.manhattan(nuevoTablero.split(","), objetivo);
				}
				if(nombreHeuristica.equals("rabon")){
					costoEstimado = heuristica.rabon(nuevoTablero.split(","), objetivo);
				}
				if(nombreHeuristica.equals("ninguna")){
					costoEstimado = heuristica.ninguna();
				}
				tableroCalculado = new Tablero(nuevoTablero, costoEstimado, costoReal);
				tableroCalculado = new Tablero(nuevoTablero, costoEstimado);
				tableroCalculado.setPadre(tablero);
				if (!tableroVisitado(tableroCalculado)) {
					tablerosPosibles.add(tableroCalculado);
				}
//				System.out.println(nuevoTablero);
			}
		}
		// Derecha
		if (indiceEspacio + 1 <= 15) {
			if (indiceEspacio != 3 && indiceEspacio != 7 && indiceEspacio != 11 && indiceEspacio != 15) {
				nuevoTablero = generarTablero(cadenaTablero, indiceEspacio, indiceEspacio + 1);
				costoReal = tablero.getCostoReal() + costoReal(cadenaTablero, nuevoTablero.split(","));
				if(nombreHeuristica.equals("manhattan")){
					costoEstimado = heuristica.manhattan(nuevoTablero.split(","), objetivo);
				}
				if(nombreHeuristica.equals("rabon")){
					costoEstimado = heuristica.rabon(nuevoTablero.split(","), objetivo);
				}
				if(nombreHeuristica.equals("ninguna")){
					costoEstimado = heuristica.ninguna();
				}
				tableroCalculado = new Tablero(nuevoTablero, costoEstimado, costoReal);
				tableroCalculado = new Tablero(nuevoTablero, costoEstimado);
				tableroCalculado.setPadre(tablero);
				if (!tableroVisitado(tableroCalculado)) {
					tablerosPosibles.add(tableroCalculado);
				}
//				System.out.println(nuevoTablero);
			}
		}

	}

	public String generarTablero(String[] cadenaTablero, Integer indiceInicial, Integer indiceFinal) {
		String resultado = "";
		String[] cadena = cadenaTablero.clone();
		String temporal = cadena[indiceFinal];
		cadena[indiceInicial] = temporal;
		cadena[indiceFinal] = "0";

		for (Integer i = 0; i < cadena.length; i++) {
			resultado = resultado + cadena[i] + ",";
		}

		return resultado.substring(0, resultado.length() - 1);
	}

	public Integer encontrarEspacio(String[] cadenaTablero) {
		Integer resultado = 0;

		for (Integer j = 0; j < cadenaTablero.length; j++) {
			if (cadenaTablero[j].equals("0")) {
				resultado = j;
				break;
			}
		}

		return resultado;
	}

	public boolean esObjetivo(Tablero tablero, Tablero objetivo) {
		boolean resultado = false;

		String contenido1 = tablero.getContenido();
		String contenido2 = objetivo.getContenido();

		if (contenido1.equals(contenido2) /*
											 * && tablerosPosibles.peek().
											 * getContenido().equals(contenido2)
											 */) {
			resultado = true;
		}

		return resultado;
	}

	public boolean tableroVisitado(Tablero nuevoTablero) {
		boolean resultado = false;
		Tablero tablero1;
		
		Iterator<Tablero> iterador = tablerosVisitados.iterator();
		while(iterador.hasNext()) {
			tablero1=iterador.next();
			if (tablero1.getContenido().equals(nuevoTablero.getContenido())) {
				if (Double.compare(tablero1.getCostoTotal(),nuevoTablero.getCostoTotal())==0) {
					resultado = true;
					break;
				}
			}
		}

		return resultado;
	}
	
	public Double costoReal(String[] tablero, String[] objetivo) {
		Double resultado = 0.0;

		Integer indiceOrigen;
		Integer indiceDestino;

		String[] puntoUno;
		String[] puntoDos;

		Double calculo = 0.0;

		for (int i = 0; i < objetivo.length; i++) {
			if (!objetivo[i].equals(tablero[i])) {
				indiceOrigen = Heuristica.encontrarIndice(tablero, objetivo[i]);
				indiceDestino = i;
				puntoUno = Heuristica.coordenadas.get(indiceOrigen).split(",");
				puntoDos = Heuristica.coordenadas.get(indiceDestino).split(",");

				calculo = Math.sqrt(Math.pow(Double.parseDouble(puntoUno[0]) - Double.parseDouble(puntoDos[0]), 2)
						+ Math.pow(Double.parseDouble(puntoUno[1]) - Double.parseDouble(puntoDos[1]), 2));

				resultado = resultado + calculo;
			}
		}

		return resultado;
	}
	
	public Integer contarPadres(Tablero tablero){
		Integer res=0;
		Tablero padre=null;
		do{
			padre=tablero.getPadre();
			tablero=padre;
			res++;
		}while(padre!=null);
		
		return res;
	}

}
