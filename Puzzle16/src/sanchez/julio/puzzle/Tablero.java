/*
 * 
 * Autor Julio Cesar Sanchez Torres 2879351
 * 
 */

package sanchez.julio.puzzle;

public class Tablero {
	
	private String contenido;
	private Double costoReal;
	private Double costoAproximado;
	private Double costoTotal;
	private Tablero padre;
	
	public Tablero(String contenido, Double costoReal, Double costoAproximado){
		this.contenido=contenido;
		this.costoReal=costoReal;
		this.costoAproximado=costoAproximado;
		this.costoTotal=costoReal+costoAproximado;
	}
	
	public Tablero(String contenido, Double costoAproximado){
		this.contenido=contenido;
		this.costoReal=0.0;
		this.costoAproximado=costoAproximado;
		this.costoTotal=costoAproximado;
	}
	
	public Tablero(String contenido){
		this.contenido=contenido;
		this.costoReal=0.0;
		this.costoTotal=costoAproximado;
	}
	
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Double getCostoReal() {
		return costoReal;
	}

	public void setCostoReal(Double costoReal) {
		this.costoReal = costoReal;
		this.costoTotal=this.costoReal+this.costoAproximado;
	}

	public Double getCostoAproximado() {
		return costoAproximado;
	}

	public void setCostoAproximado(Double costoAproximado) {
		this.costoAproximado = costoAproximado;
		this.costoTotal=this.costoReal+this.costoAproximado;
	}

	public Double getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(Double costoTotal) {
		this.costoTotal = costoTotal;
	}

	public Tablero getPadre() {
		return padre;
	}

	public void setPadre(Tablero padre) {
		this.padre = padre;
	}

}
